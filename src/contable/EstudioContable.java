/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package contable;

import contable.persistencia.ArrayCuentaDAO;
import contable.persistencia.ICuentaDAO;

/**
 *
 * @author Usuario
 */
public class EstudioContable {
    
    private static EstudioContable estudoContable;
    
    private GestorCuentas gestorCuentas;
    
    private EstudioContable(ICuentaDAO cuentaDAO){
        this.gestorCuentas = new GestorCuentas(cuentaDAO);
    }

    public static EstudioContable intancia(){
        if (estudoContable == null) {
            ICuentaDAO cuentaDAO = new ArrayCuentaDAO();
            estudoContable = new EstudioContable(cuentaDAO);
        }
        return estudoContable;
    }
    
    public GestorCuentas getGestorCuentas(){
        return gestorCuentas;
    }
}
